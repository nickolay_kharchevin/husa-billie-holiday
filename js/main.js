// Navigation
var navOpenBtn = $('#nav-open-btn');
var navigationWrapper = $('#navigation-wrapper');
// Navigation Links
var homeLink = $('#home-link');
var festivalLink = $('#festival-link');
var mediaLink = $('#media-link');

// Video Overlay
var playVideoBtn = $('#play-video-btn');
var closeVideoBtn = $('#close-video-btn');
var desktopVideoOverlay = $('#desktop-video-overlay');

// Contact Form
var showFormButton = $('#show-form-btn');
var contactFormOverlay = $('#contact-form-overlay');
var closeFormButton = $('#close-form-btn');

// Views
var homePage = $('#home-page');
var festivalPage = $('#festival-page');
var mediaPage = $('#media-page');

// Transition
var transition = $('#transition');

// Slide Toggle On Menu
navOpenBtn.click(function() {
    navigationWrapper.slideToggle();
});


showFormButton.click(function() {
    contactFormOverlay.animate(
        {
            opacity: 1
        },
        {
            start: function() {
                contactFormOverlay.css('zIndex', 2000);
            },
            duration: 500
        }
    )
});

closeFormButton.click(function() {
    contactFormOverlay.animate(
        {
            opacity: 0
        },
        {
            duration: 500,
            complete: function() {
                contactFormOverlay.css('zIndex', '-1');
            }
        }
    )
});
