// ONLY USED FOR MOBILE ROUTER
var currentMobileView = homePage;
homeLink.click(function() {
    changeMobileView(homePage);
    currentMobileView = homePage;
});

festivalLink.click(function() {
    changeMobileView(festivalPage);
    currentMobileView = festivalPage;
});

mediaLink.click(function() {
    changeMobileView(mediaPage);
    currentMobileView = mediaPage;
});

var changeMobileView = function(divToShow) {
    currentMobileView.css({ 'display': 'none', 'opacity': 0 });
    divToShow.css({ 'display': 'block', 'opacity': 1 });
};

// Open Video
playVideoBtn.click(function() {
    desktopVideoOverlay.animate(
        {
            opacity: 1
        },
        {
            start: function() {
                desktopVideoOverlay.css('zIndex', 2000);
            },
            duration: 500
        }
    )
});

// Close Video
closeVideoBtn.click(function() {
    $("video").each(function () { this.pause() });
    desktopVideoOverlay.animate(
        {
            opacity: 0
        },
        {
            duration: 500,
            complete: function() {
                desktopVideoOverlay.css('zIndex', '-1');
            }
        }
    )
});
