// Instantiate Parallax Scenes

// Initialize home-page-scene parallax
$('#home-page-scene').parallax();
$('#media-page-scene').parallax();
$('#festival-page-scene').parallax();

$(document).ready(function() {
    homePage.vide(
        {
            mp4: 'media/videos/home-page-bg-movie.mp4',
            poster: 'media/images/posters/home-page-bg-poster.jpg'
        },
        {
            autoplay: true,
            loop: true,
            muted: true
        }
    );

    festivalPage.vide(
        {
            mp4: 'media/videos/festival-page-bg-movie.mp4',
            poster: 'media/images/posters/festival-movie-bg-poster.jpg'
        },
        {
            autoplay: true,
            loop: true,
            muted: true
        }
    )

});

// Open Video
playVideoBtn.click(function() {
    desktopVideoOverlay.animate(
        {
            opacity: 1
        },
        {
            start: function() {
                desktopVideoOverlay.css('zIndex', 2000);
            },
            duration: 500
        }
    )
});

// Close Video
closeVideoBtn.click(function() {
    $("video").each(function () { this.pause() });
    desktopVideoOverlay.animate(
        {
            opacity: 0
        },
        {
            duration: 500,
            complete: function() {
                desktopVideoOverlay.css('zIndex', '-1');
            }
        }
    )
});

// Close Video With esc
$(document).keyup(function(e) {
    if (e.keyCode === 27) closeVideoBtn.click();
});

// Router
// Set Initial Div To Show
var currentView = homePage;

homeLink.click(function(e) {
    changeView(homePage);
    currentView = homePage;
});

festivalLink.click(function() {
    changeView(festivalPage);
    currentView = festivalPage;
});

mediaLink.click(function() {
    changeView(mediaPage);
    currentView = mediaPage;
});

var changeView = function(divToShow) {
    // stop current animation if any
    currentView.stop();

    // Remove Current View
    currentView.css({ 'display': 'none', 'opacity': 0 });
    // Bring up transition Div
    transition.css({ 'display': 'block', 'opacity': 1});

    // Find out what transition to show based on the currentView
    // currentView then currentView's transition then divToShow
    var movie;
    var poster;

    if (currentView === homePage) {
        movie = 'media/videos/home-page-transition.mp4';
        poster = 'media/images/posters/home-page-transition-poster.jpeg';
    } else if (currentView === festivalPage) {
        movie = 'media/videos/festival-page-transition.mp4';
        poster = 'media/images/posters/festival-page-transition-poster.jpeg'
    }   else if (currentView === mediaPage) {
        movie = 'media/videos/media-page-transition.mp4';
        poster = 'media/images/posters/media-page-transition-poster.jpeg'
    }


    // Set the transition up
    // Now we set our transitional state
    // For production set muted to false
    transition.vide({mp4: movie, poster: poster}, {autoplay: true, loop: false, muted: false});

    transition.animate(
        {
            opacity: 0
        },
        {
            duration: 5500,
            start: function() {
                divToShow.css('display', 'block');
            },
            complete: function() {
                transition.css('display', 'none');
                divToShow.animate({opacity: 1}, 1000);

                var bgMovie;
                var bgPoster;
                // Instantiate Proper Background Video
                if (divToShow === homePage) {
                    bgMovie = 'media/videos/home-page-bg-movie.mp4';
                    bgPoster = 'media/images/posters/home-page-transition-poster.jpeg';
                } else if (divToShow === festivalPage) {
                    bgMovie = 'media/videos/festival-page-bg-movie.mp4';
                    bgPoster = 'media/images/posters/festival-page-transition-poster.jpeg'
                }

                if (divToShow === homePage || divToShow === festivalPage) {
                    divToShow.vide({mp4: bgMovie, poster: bgPoster},{ autoplay: true, loop: true, muted: true});
                }

            }
        }
    )
};